#!/bin/bash
# shellcheck disable=SC1090,SC1091

# Strict error handling
set -euo pipefail
IFS=$'\n\t'

# User ~/.dotfiles directory to be in line with prevailing conventions
DOTFILES_DIR="${DOTFILES_DIR:-$HOME/.dotfiles}"

# Logging setup
readonly LOG_FILE="${DOTFILES_DIR:-$HOME/.dotfiles}/loki-shell-install.log"
mkdir -p "$(dirname "$LOG_FILE")"

log() {
    local level="$1"
    shift
    printf '[%s] [%s]: %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$level" "$*" | tee -a "$LOG_FILE"
}

die() {
    log "ERROR" "$*"
    exit 1
}

# Cleanup handler
cleanup() {
    local exit_code=$?
    if [ $exit_code -ne 0 ]; then
        log "ERROR" "Installation failed with exit code $exit_code. Check $LOG_FILE for details."
    fi
}
trap cleanup EXIT

# Function to symlink files
link_files() {
    local source_file="$1"
    local target_file="$2"
    
    if [ ! -e "$source_file" ]; then
        die "Source file does not exist: $source_file"
    fi
    
    # Remove existing file/symlink
    rm -f "$target_file"
    
    # Create symlink
    log "INFO" "Creating symlink: $target_file -> $source_file"
    ln -s "$source_file" "$target_file" || die "Failed to create symlink $target_file"
}

# Function to add source command if not present
add_source_if_missing() {
    local file="$1"
    local source_cmd="$2"
    
    # Create file if it doesn't exist
    touch "$file" || die "Failed to create/touch $file"
    
    # Add source command if not already present
    if ! grep -Fxq "$source_cmd" "$file"; then
        log "INFO" "Adding to $file: $source_cmd"
        echo "$source_cmd" >> "$file" || die "Failed to update $file"
    fi
}

# Setup shell config files
log "INFO" "Setting up shell configurations..."

# Backup existing bashrc if it exists
if [[ -f "$HOME/.bashrc" ]]; then
    log "INFO" "Backing up existing .bashrc"
    mv "$HOME/.bashrc" "$HOME/bashrc.backup" || die "Failed to backup .bashrc"
fi

# Create minimal bashrc that sources our config
log "INFO" "Creating new .bashrc"
cat > "$HOME/.bashrc" << 'EOF' || die "Failed to create .bashrc"
# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Source shell extensions
if [ -f "${DOTFILES_DIR:-$HOME/.dotfiles}/bash/bashrc" ]; then
    . "${DOTFILES_DIR:-$HOME/.dotfiles}/bash/bashrc"
fi
EOF

# Ensure bash_profile sources bashrc
add_source_if_missing "$HOME/.bash_profile" '[[ -f ~/.bashrc ]] && . ~/.bashrc'

# Setup symlinks for various config files
log "INFO" "Setting up symlinks..."

# Nano config
link_files "$DOTFILES_DIR/nano/nanorc" "$HOME/.nanorc"

# SSH config
#mkdir -p "$HOME/.sshrc.d" || die "Failed to create .sshrc.d directory"
#ln -s "${DOTFILES_DIR}/sshrc/sshrc.d" "$HOME/.sshrc.d" || die "Failed to create .sshrc.d directory"
#link_files "$DOTFILES_DIR/sshrc/sshrc" "$HOME/.sshrc"

# TMux config
#if [[ ! -f "$HOME/.tmux.conf" ]]; then
#    touch "$HOME/.tmux.conf" || die "Failed to create .tmux.conf"
#    link_files "$DOTFILES_DIR/tmux/tmux.conf.common" "$HOME/.tmux.conf"
#else
	#add_source_if_missing "$HOME/.tmux.conf" "source-file ${DOTFILES_DIR}/tmux/tmux.conf.common"
#fi

# Git config
#git config --global core.excludesfile "$DOTFILES_DIR/git/gitignore_global" || log "WARN" "Failed to set git excludesfile"

# Setup ZSH configuration if zsh is installed
if command -v zsh >/dev/null 2>&1; then
    log "INFO" "Setting up Zsh configuration..."
    if [[ ! -d "$HOME/.oh-my-zsh/themes" ]]; then
        mkdir -p "$HOME/.oh-my-zsh/themes" || die "Failed to create oh-my-zsh themes directory"
    fi
    link_files "$DOTFILES_DIR/zsh/loki.zsh-theme" "$HOME/.oh-my-zsh/themes/loki.zsh-theme"
    link_files "$DOTFILES_DIR/zsh/zshrc" "$HOME/.zshrc"
fi

log "INFO" "Installation complete! Please restart your shell or run 'source ~/.bashrc'"
