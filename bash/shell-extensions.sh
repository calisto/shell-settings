#!/bin/bash
# shellcheck disable=SC2139,SC2140,SC2142

# ====================
# Environment Settings
# ====================

export BLOCKSIZE=1k

# ====================
# Directory Navigation
# ====================

# Enhanced directory stack management
pushd() {
    local dir="${1:-$HOME}"
    if ! builtin pushd "${dir}" > /dev/null 2>&1; then
        log "ERROR" "Failed to pushd to ${dir}"
        return 1
    fi
}

pushd_builtin() {
    if ! builtin pushd > /dev/null 2>&1; then
        log "ERROR" "Failed to pushd"
        return 1
    fi
}

popd() {
    if ! builtin popd > /dev/null 2>&1; then
        log "ERROR" "Failed to popd"
        return 1
    fi
}

# Directory navigation aliases
alias cd='pushd'
alias back='popd'
alias flip='pushd_builtin'
alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ~="cd ~"

# ====================
# File Operations
# ====================

# Safe file operation aliases
alias cp='cp -iv'
alias mv='mv -iv'
alias mkdir='mkdir -pv'
alias rm='rm -v'
alias ln='ln -iv'

# Enhanced ls aliases
if command -v exa >/dev/null 2>&1; then
    alias ll='exa -l --git --icons'
    alias la='exa -la --git --icons'
    alias lt='exa -T --git-ignore --icons'
else
    alias ls='ls --color=auto'
#    alias ll='ls -FGlAhp'                       # Preferred 'ls' implementation
    alias ll='ls -FGlAhp'
    alias la='ls -FGlAhp'
fi

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Directory size analysis
alias ducks='du -chs * | sort -rh | head'
alias space='du -Sh | sort -rh | head -20'

# Create directory and enter it
mcd() {
    if [[ $# -ne 1 ]]; then
        log "ERROR" "Usage: mcd <directory>"
        return 1
    fi
    if mkdir -p "$1"; then
        cd "$1" || return 1
    fi
}

# Archive operations
zipf() {
    local archive_name
    if [[ $# -ne 1 ]]; then
        log "ERROR" "Usage: zipf <directory>"
        return 1
    fi
    archive_name="${1%/}.zip"
    zip -r "$archive_name" "$1"
}

# Enhanced archive extraction
extract() {
    if [[ $# -ne 1 ]]; then
        log "ERROR" "Usage: extract <archive>"
        return 1
    fi

    if [[ ! -f "$1" ]]; then
        log "ERROR" "File '$1' does not exist"
        return 1
    fi

    case "$1" in
        *.tar.bz2|*.tbz2) tar xjf "$1" ;;
        *.tar.gz|*.tgz)   tar xzf "$1" ;;
        *.tar.xz)         tar xJf "$1" ;;
        *.bz2)            bunzip2 "$1" ;;
        *.rar)            unrar x "$1" ;;
        *.gz)             gunzip "$1" ;;
        *.tar)            tar xf "$1" ;;
        *.zip)            unzip "$1" ;;
        *.Z)              uncompress "$1" ;;
        *.7z)             7z x "$1" ;;
        *)                log "ERROR" "'$1' cannot be extracted via extract()" 
                         return 1 ;;
    esac
}

# ====================
# System Operations
# ====================

# Terminal improvements
alias c='clear'
alias path='printf "%s\n" "${PATH//:/$'\n'}"'
alias fix_stty='stty sane'
alias show_options='shopt'

# Process management
findpid() {
    local pattern="$1"
    if [[ -z "$pattern" ]]; then
        log "ERROR" "Usage: findpid <pattern>"
        return 1
    fi
    pgrep -fl "$pattern"
}

my_ps() {
    ps "${@:--ef}" -u "$USER" -o pid,%cpu,%mem,start,time,command
}

# System monitoring
alias cpu_hogs='ps -eo pid,pcpu,comm --no-headers | sort -t. -nk2 -r | head -10'
alias mem_hogs='ps -eo pid,pmem,comm --no-headers | sort -t. -nk2 -r | head -10'
alias disk_hogs='du -hsx * | sort -rh | head -10'

# ====================
# File Search
# ====================

# Enhanced find commands
ff() { find . -type f -iname "*$1*" ; }
ffd() { find . -type d -iname "*$1*" ; }

# Recursive file search with content
search() {
    if [[ $# -eq 0 ]]; then
        log "ERROR" "Usage: search <pattern>"
        return 1
    fi
    if command -v rg >/dev/null 2>&1; then
        rg --color=always --line-number --no-heading --smart-case "${*:-}"
    else
        grep -r --color=always --line-number --no-messages "$@" .
    fi
}

# ====================
# Network Operations
# ====================

# IP address operations
myip() {
    local ip
    ip=$(curl -s -m 5 https://api.ipify.org) || ip=$(curl -s -m 5 http://whatismyip.akamai.com/)
    echo "${ip:-Could not determine IP address}"
}

# Network monitoring
alias netcons='ss -tulanp'
alias listening='ss -tlnp'
alias ips='ip -c a'

# HTTP operations
httpheaders() {
    if [[ $# -eq 0 ]]; then
        log "ERROR" "Usage: httpheaders <url>"
        return 1
    fi
    curl -sSL -D - "$1" -o /dev/null
}

httpdebug() {
    if [[ $# -eq 0 ]]; then
        log "ERROR" "Usage: httpdebug <url>"
        return 1
    fi
    curl -sSL "$1" -o /dev/null -w "dns: %{time_namelookup}s\nconnect: %{time_connect}s\nssl: %{time_appconnect}s\npretransfer: %{time_pretransfer}s\nstarttransfer: %{time_starttransfer}s\ntotal: %{time_total}s\n"
}

# ====================
# Development Tools
# ====================

# Git shortcuts
if command -v git >/dev/null 2>&1; then
    alias g='git'
    alias gs='git status'
    alias gd='git diff'
    alias gl='git log --oneline'
fi

# Docker shortcuts
if command -v docker >/dev/null 2>&1; then
    alias d='docker'
    alias dc='docker-compose'
    alias dps='docker ps'
    alias di='docker images'
fi

# ====================
# System Information
# ====================

sysinfo() {
    printf "\n"
    printf "System Information:\n"
    printf "==================\n"
    printf "Hostname: %s\n" "$(hostname)"
    printf "Kernel: %s\n" "$(uname -r)"
    printf "Uptime: %s\n" "$(uptime -p)"
    printf "Shell: %s\n" "$SHELL"
    printf "Terminal: %s\n" "$TERM"
    printf "\n"
    printf "CPU Usage: %s\n" "$(top -bn1 | grep "Cpu(s)" | awk '{print $2 + $4}')%"
    printf "Memory Usage: %s\n" "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2}')"
    printf "Disk Usage: %s\n" "$(df -h / | awk 'NR==2{print $5}')"
    printf "\n"
    myip
    printf "\n"
}

# ====================
# Cleanup Operations
# ====================

# Cleanup temporary files
cleanup() {
    find "${TMPDIR:-/tmp}" -type f -atime +7 -delete 2>/dev/null
    find "${TMPDIR:-/tmp}" -type d -empty -delete 2>/dev/null
}

# OS-specific cleanup
if [[ "$OSTYPE" == "darwin"* ]]; then
    alias cleanup_ds="find . -type f -name '*.DS_Store' -ls -delete"
fi
