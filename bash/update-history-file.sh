#!/bin/bash

# Log function
__update_history_log() {
    local level="$1"
    shift
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] [$level]: $*"
}

__update_history() {
	# Define history file paths
	local CUSTOM_HISTORY="${HOME}/.q_bash_history"
	local DEFAULT_HISTORY="${HOME}/.bash_history"
	local BACKUP_HISTORY="${HOME}/.bash_history.bak"
	local CUSTOM_BACKUP_HISTORY="${HOME}/.q_bash_history.bak"

	if [[ -f "$CUSTOM_BACKUP_HISTORY" ]]; then
		# Already done, goodbye!
		return
	fi

	# Check if the custom history file exists
	if [[ -f "$CUSTOM_HISTORY" ]]; then
		__update_history_log "INFO" "Custom history file found. Moving to ~/.bash_history."

		# Backup existing .bash_history just in case
		cp "$DEFAULT_HISTORY" "$BACKUP_HISTORY"
		__update_history_log "INFO" "Backup of .bash_history created at $BACKUP_HISTORY."
		
		# Backup existing .q_bash_history just in case
		cp "$CUSTOM_HISTORY" "$CUSTOM_BACKUP_HISTORY"
		__update_history_log "INFO" "Backup of .q_bash_history created at $BACKUP_HISTORY."

		rm $DEFAULT_HISTORY
		mv $CUSTOM_HISTORY $DEFAULT_HISTORY

		# Ensure correct permissions
		chmod 600 "$DEFAULT_HISTORY"

		__update_history_log "INFO" "Updated history and removed $CUSTOM_HISTORY."
	# else
		# __update_history_log "INFO" "No custom history file found. No changes made."
	fi
}

# Execute update
__update_history

# Be kind, rewind.
unset -f __update_history
unset -f __update_history_log
