#!/bin/bash

# Text formatting variables using tput
BOLD="$(tput bold)"
DIM="$(tput dim)"
UNDERLINE_ON="$(tput smul)"
UNDERLINE_OFF="$(tput rmul)"
REVERSE="$(tput rev)"
STANDOUT_ON="$(tput smso)"
STANDOUT_OFF="$(tput rmso)"

# Foreground (text) colors
FG_BLACK="$(tput setaf 0)"
FG_RED="$(tput setaf 1)"
FG_GREEN="$(tput setaf 2)"
FG_YELLOW="$(tput setaf 3)"
FG_BLUE="$(tput setaf 4)"
FG_MAGENTA="$(tput setaf 5)"
FG_CYAN="$(tput setaf 6)"
FG_WHITE="$(tput setaf 7)"

# Background colors
BG_BLACK="$(tput setab 0)"
BG_RED="$(tput setab 1)"
BG_GREEN="$(tput setab 2)"
BG_YELLOW="$(tput setab 3)"
BG_BLUE="$(tput setab 4)"
BG_MAGENTA="$(tput setab 5)"
BG_CYAN="$(tput setab 6)"
BG_WHITE="$(tput setab 7)"

# Reset color to default
RESET_COLOR="$(tput sgr0)"

# Prompt-safe versions (wrapped in \[ \] to avoid cursor issues in PS1)
BOLD_ESCAPED="\[${BOLD}\]"
DIM_ESCAPED="\[${DIM}\]"
UNDERLINE_ON_ESCAPED="\[${UNDERLINE_ON}\]"
UNDERLINE_OFF_ESCAPED="\[${UNDERLINE_OFF}\]"
REVERSE_ESCAPED="\[${REVERSE}\]"
STANDOUT_ON_ESCAPED="\[${STANDOUT_ON}\]"
STANDOUT_OFF_ESCAPED="\[${STANDOUT_OFF}\]"

# Foreground colors - prompt safe
FG_BLACK_ESCAPED="\[${FG_BLACK}\]"
FG_RED_ESCAPED="\[${FG_RED}\]"
FG_GREEN_ESCAPED="\[${FG_GREEN}\]"
FG_YELLOW_ESCAPED="\[${FG_YELLOW}\]"
FG_BLUE_ESCAPED="\[${FG_BLUE}\]"
FG_MAGENTA_ESCAPED="\[${FG_MAGENTA}\]"
FG_CYAN_ESCAPED="\[${FG_CYAN}\]"
FG_WHITE_ESCAPED="\[${FG_WHITE}\]"

# Background colors - prompt safe
BG_BLACK_ESCAPED="\[${BG_BLACK}\]"
BG_RED_ESCAPED="\[${BG_RED}\]"
BG_GREEN_ESCAPED="\[${BG_GREEN}\]"
BG_YELLOW_ESCAPED="\[${BG_YELLOW}\]"
BG_BLUE_ESCAPED="\[${BG_BLUE}\]"
BG_MAGENTA_ESCAPED="\[${BG_MAGENTA}\]"
BG_CYAN_ESCAPED="\[${BG_CYAN}\]"
BG_WHITE_ESCAPED="\[${BG_WHITE}\]"

# Reset color to default - prompt safe
RESET_ESCAPED="\[${RESET_COLOR}\]"

# Full Reset (both versions)
FULL_RESET="${STANDOUT_OFF}${RESET_COLOR}"
FULL_RESET_ESCAPED="\[${STANDOUT_OFF}${RESET_COLOR}\]"

# Export both raw and prompt-safe versions
export BOLD_RAW DIM_RAW UNDERLINE_ON_RAW UNDERLINE_OFF_RAW REVERSE_RAW STANDOUT_ON_RAW STANDOUT_OFF_RAW
export FG_BLACK_RAW FG_RED_RAW FG_GREEN_RAW FG_YELLOW_RAW FG_BLUE_RAW FG_MAGENTA_RAW FG_CYAN_RAW FG_WHITE_RAW
export BG_BLACK_RAW BG_RED_RAW BG_GREEN_RAW BG_YELLOW_RAW BG_BLUE_RAW BG_MAGENTA_RAW BG_CYAN_RAW BG_WHITE_RAW
export RESET_COLOR_RAW

export BOLD DIM UNDERLINE_ON UNDERLINE_OFF REVERSE STANDOUT_ON STANDOUT_OFF
export FG_BLACK FG_RED FG_GREEN FG_YELLOW FG_BLUE FG_MAGENTA FG_CYAN FG_WHITE
export BG_BLACK BG_RED BG_GREEN BG_YELLOW BG_BLUE BG_MAGENTA BG_CYAN BG_WHITE
export RESET_COLOR
