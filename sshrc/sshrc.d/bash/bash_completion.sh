#!/bin/bash

# Common locations for bash completion
COMPLETION_PATHS=(
    "/usr/local/etc/profile.d/bash_completion.sh"    # Homebrew (macOS)
    "/usr/share/bash-completion/bash_completion"      # Linux (common)
    "/etc/bash_completion"                           # Linux (old-style)
    "/usr/local/share/bash-completion/bash_completion" # Alternative Linux
)

# Try to find and source bash completion
for completion_file in "${COMPLETION_PATHS[@]}"; do
    if [[ -r "$completion_file" ]]; then
        # Source the first bash completion file found
        . "$completion_file"
        break
    fi
done

# Note: It's normal for bash completion to not be available on all systems,
# so we don't need to log any errors if none are found.
