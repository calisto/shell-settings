#!/usr/bin/env bash

###############################################################################
# 1. Check for bash-preexec and load it if needed
###############################################################################
# Make sure it's already sourced and installed
if ! declare -F __bp_install > /dev/null 2>&1; then
    echo "ERROR: bash-preexec is not loaded. Please source bash-preexec.sh first." >&2
    return 1
fi

# Force reinstall of bash-preexec to ensure hooks are properly set up
__bp_install

###############################################################################
# 2. Configuration (DB file + table)
###############################################################################
SHELL_HISTORY_DATABASE_FILE="${SHELL_HISTORY_DATABASE_FILE:-$HOME/.tracked-history.db}"
HISTORY_TABLE_NAME="shell_history"
HISTORY_DEBUG="${HISTORY_DEBUG:-0}"

# Debug logging function
__history_debug() {
    if [[ "${HISTORY_DEBUG}" == "1" ]]; then
        printf '[DEBUG] [%s]: %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$*" >&2
    fi
}

sqlite3 "$SHELL_HISTORY_DATABASE_FILE" >/dev/null <<SQL
PRAGMA secure_delete = ON;
PRAGMA journal_mode = WAL;

CREATE TABLE IF NOT EXISTS $HISTORY_TABLE_NAME (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    full_command TEXT NOT NULL,
    exit_code INTEGER DEFAULT -1,
    duration_ms INTEGER,
    insertion_ts DATETIME DEFAULT CURRENT_TIMESTAMP, -- UTC time at insert
    command_start_ts DATETIME,                       -- UTC time the command began
    cwd TEXT
);
SQL

###############################################################################
# 3. Variables (EPOCHREALTIME usage + our global placeholders)
###############################################################################
# Initialize with invalid values to detect first-run
COMMAND_START_TIME=-1
LAST_COMMAND_LINE=""
LAST_EXIT_CODE=-1
COMMAND_RUNNING=0

###############################################################################
# 4. Our preexec/precmd hooks
###############################################################################
# This function runs right before executing a user command.
# $1 is the command line that the user typed.
preexec_hook() {
    __history_debug "preexec_hook called with: $1"

    # Skip if command is empty or just whitespace
    if [[ -z "${1// }" ]]; then
        __history_debug "Empty command, skipping"
        return
    fi

    # Set command info first, then mark as running
    LAST_COMMAND_LINE="$1"
    COMMAND_START_TIME="$EPOCHREALTIME"
    COMMAND_RUNNING=1

    __history_debug "Command captured: $LAST_COMMAND_LINE"
    __history_debug "Start time: $COMMAND_START_TIME"
}

# This function runs just before Bash shows the next prompt
precmd_hook() {
    # Capture exit code immediately
    LAST_EXIT_CODE=$?

    __history_debug "precmd_hook called"

    # Skip if no command was running
    if [[ "$COMMAND_RUNNING" != "1" ]]; then
        __history_debug "No command was running, skipping"
        return
    fi

    if [[ -n "${__bp_last_ret_value:-}" ]]; then
        LAST_EXIT_CODE="$__bp_last_ret_value"
    fi

    __history_debug "Exit code: $LAST_EXIT_CODE"

    # Skip if we don't have a valid command
    if [[ -z "$LAST_COMMAND_LINE" || "$COMMAND_START_TIME" == "-1" ]]; then
        __history_debug "No valid command or start time, skipping"
        return
    fi

    local end_time="$EPOCHREALTIME"
    local cwd="$PWD"

    __history_debug "End time: $end_time"

    # Convert float to integer microseconds:
    local start_us end_us
    start_us=$(float_to_microseconds "$COMMAND_START_TIME")
    end_us=$(float_to_microseconds "$end_time")

    # Duration in ms
    local duration_ms=$(( (end_us - start_us) / 1000 ))

    # We'll store command_start_ts using the integer epoch (seconds)
    local start_sec="${COMMAND_START_TIME%%.*}"

    # Insert into SQLite:
    __history_debug "Inserting command into database with exit code: $LAST_EXIT_CODE"

    sqlite3 "$SHELL_HISTORY_DATABASE_FILE" <<SQL
INSERT INTO $HISTORY_TABLE_NAME (
    full_command,
    exit_code,
    duration_ms,
    command_start_ts,
    cwd
)
VALUES (
    '${LAST_COMMAND_LINE//\'/''}',
    $LAST_EXIT_CODE,
    $duration_ms,
    datetime($start_sec, 'unixepoch'),
    '${cwd//\'/''}'
);
SQL

    # Reset state
    LAST_COMMAND_LINE=""
    COMMAND_START_TIME=-1
    COMMAND_RUNNING=0
    LAST_EXIT_CODE=-1

    __history_debug "State reset complete"
}

###############################################################################
# 5. float_to_microseconds (same as before)
###############################################################################
float_to_microseconds() {
    local float="$1"
    # Separate integer part (seconds) from fractional (microseconds)
    local sec="${float%%.*}"
    local frac="${float#*.}"
    frac="$(printf '%-6s' "$frac" | tr ' ' 0)"
    echo $(( 10#$sec * 1000000 + 10#$frac ))
}

# Handle command logging on shell exit
exit_trap_hook() {
    __history_debug "Exit trap called"

    # Only process if we have a command running
    if [[ "$COMMAND_RUNNING" == "1" ]]; then
        __history_debug "Command was running, calling precmd_hook"
        precmd_hook
    fi

    # Reset all state variables
    LAST_COMMAND_LINE=""
    COMMAND_START_TIME=-1
    COMMAND_RUNNING=0
    LAST_EXIT_CODE=-1

    __history_debug "Exit trap complete"
}

# B) install the trap
trap 'exit_trap_hook' EXIT

###############################################################################
# 6. Register these hooks with bash-preexec
###############################################################################
# Remove any existing hooks first to prevent duplicates
preexec_functions=(${preexec_functions[@]/preexec_hook})
precmd_functions=(${precmd_functions[@]/precmd_hook})

# Add our hooks
preexec_functions+=(preexec_hook)
precmd_functions+=(precmd_hook)

###############################################################################
# 6. Query function (hquery)
###############################################################################
hquery() {
    local table="$HISTORY_TABLE_NAME"
    local where_clauses=()
    local pattern
    local order="ORDER BY insertion_ts ASC"
    local limit="LIMIT 10000"
    local group_mode=0
    local freq_order="ASC"  # default for --mfu => largest usage last
    local recent_query=0    # new flag for --recent
    local recent_limit=

    while [ $# -gt 0 ]; do
        case "$1" in
            --help)
                hreadme
                return
                ;;
            --exit)
                where_clauses+=("exit_code = $2")
                shift 2
                ;;
            --id)
                local id="$2"
                sqlite3 "$SHELL_HISTORY_DATABASE_FILE" "SELECT full_command FROM $HISTORY_TABLE_NAME WHERE id = $id;"
                shift 2
                return
                ;;
            --after|--before)
                local operator=">="  # Default to 'after'
                [[ "$1" == "--before" ]] && operator="<="
                if [[ "$2" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2} ]]; then
                    where_clauses+=("insertion_ts $operator datetime('$2')")
                else
                    where_clauses+=("insertion_ts $operator datetime('$(htime "$2")', 'utc')")
                fi
                shift 2
                ;;
            --success|--succeeded)
                where_clauses+=("exit_code = 0")
                shift
                ;;
            --failed|--fail|--failure)
                where_clauses+=("exit_code != 0")
                shift
                ;;
            --canceled|--cancelled|--cancel|--stopped)
                where_clauses+=("exit_code IN (130, 143, 137)")
                shift
                ;;
            --slow|--fast)
                if [ "$1" == "--slow" ]; then
                    order="ORDER BY duration_ms ASC"
                else
                    order="ORDER BY duration_ms DESC"
                fi
                shift
                ;;
            --mru)
                order="ORDER BY insertion_ts ASC"  # newest is last
                shift
                ;;
            --lru)
                order="ORDER BY insertion_ts DESC" # oldest is last
                shift
                ;;
            --mfu)
                group_mode=1
                freq_order="ASC"  # biggest usage last
                shift
                ;;
            --lfu)
                group_mode=1
                freq_order="DESC" # smallest usage last
                shift
                ;;
            -n)
                limit="LIMIT $2"
                shift 2
                ;;
            --cwd)
                if [[ -n "$2" && "$2" != -* ]]; then
                    local search_path="$2"
                    search_path="${search_path//\'/''}"
                    if [[ "$search_path" == *"%"* || "$search_path" == *"_"* || "$search_path" == *"*"* ]]; then
                        search_path="${search_path//\*/%}"
                        where_clauses+=("cwd LIKE '%${search_path}%'")
                    else
                        where_clauses+=("cwd = '${search_path}'")
                    fi
                    shift 2
                else
                    local cur_dir="$PWD"
                    cur_dir="${cur_dir//\'/''}"
                    where_clauses+=("cwd = '${cur_dir}'")
                    shift 1
                fi
                ;;
            --recent)
                recent_query=1
                if [[ -n "$2" && "$2" =~ ^[0-9]+$ ]]; then
                    recent_limit="$2"
                    shift 2
                else
                    recent_limit=25
                    shift 1
                fi
                ;;
            *)
                pattern="full_command LIKE '%${1//\*/%}%'"
                shift
                ;;
        esac
    done

    # Build the WHERE clause
    local where="1=1"
    for w in "${where_clauses[@]}"; do
        where="$where AND $w"
    done
    if [ -n "$pattern" ]; then
        where="$where AND $pattern"
    fi

    local common_query=$(cat <<SQL
id,
CASE exit_code
    WHEN 0 THEN '✅ Success'
    WHEN 130 THEN '🚫 Cancelled '
    WHEN 143 THEN '🚫 Cancelled '
    WHEN 137 THEN '🚫 Cancelled '
    ELSE '❌ Failed'
END AS status,
replace(
    replace(full_command, char(92)||char(10), ' '),
    char(10),
    ' '
) AS command,
cwd,
CASE
    WHEN duration_ms < 1000 THEN duration_ms || 'ms'
    WHEN duration_ms < 60000 THEN printf('%d', round(duration_ms / 1000.0)) || 's'
    WHEN duration_ms < 3600000 THEN printf('%d', round(duration_ms / 60000.0)) || 'm'
    WHEN duration_ms < 86400000 THEN printf('%d', round(duration_ms / 3600000.0)) || 'h'
    WHEN duration_ms < 604800000 THEN printf('%d', round(duration_ms / 86400000.0)) || 'd'
    WHEN duration_ms < 2592000000 THEN printf('%d', round(duration_ms / 604800000.0)) || 'w'
    WHEN duration_ms < 31536000000 THEN printf('%d', round(duration_ms / 2592000000.0)) || 'mo'
    ELSE printf('%d', round(duration_ms / 31536000000.0)) || 'y'
END AS duration,
CASE 
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 2 
        THEN 'just now'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 60 
        THEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) || 's ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 90 
        THEN '1m ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 3600 
        THEN printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/60.0)) || 'm ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 5400 
        THEN '1h ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 86400 
        THEN printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/3600.0)) || 'h ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 172800 
        THEN '1d ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 604800 
        THEN printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/86400.0)) || 'd ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 2592000 
        THEN printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/604800.0)) || 'w ago'
    WHEN (strftime('%s', 'now') - strftime('%s', datetime(insertion_ts))) < 31536000 
        THEN printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/2592000.0)) || 'mo ago'
    ELSE 
        printf('%d', round((strftime('%s', 'now') - strftime('%s', datetime(insertion_ts)))/31536000.0)) || 'y ago'
END AS relative_time
SQL
)

    # Define the multi-character delimiter
    DELIM="@@:DELIMA:@@"

    if [ "$group_mode" -eq 1 ]; then
        # Grouped query for MFU or LFU
        sqlite3 -header -column "$SHELL_HISTORY_DATABASE_FILE" <<SQL
SELECT
    COUNT(*) AS usage_count,
    full_command AS command
FROM $table
WHERE $where
GROUP BY full_command
ORDER BY usage_count $freq_order
$limit;
SQL

    elif [ "$recent_query" -eq 1 ]; then
        # Use a subquery to get the most recent N items, then order ascending
        sqlite3 "$SHELL_HISTORY_DATABASE_FILE" <<SQL | while read -r line; do
.mode list
.separator "$DELIM"
SELECT
    $common_query
FROM (
    SELECT id, exit_code, full_command, cwd, duration_ms, insertion_ts
    FROM $table
    WHERE $where
    ORDER BY insertion_ts DESC
    LIMIT $recent_limit
) sub
ORDER BY insertion_ts ASC;
SQL
            # Split the line using the multi-character delimiter stored in $DELIM
            id="${line%%$DELIM*}"
            rest="${line#*$DELIM}"
            status="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            full_command="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            cwd="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            duration="${rest%%$DELIM*}"
            relative_time="${rest#*$DELIM}"
            print_history_record_verbose "$id" "$status" "$full_command" "$cwd" "$duration" "$relative_time"
        done

    else
        # Standard query branch
        sqlite3 "$SHELL_HISTORY_DATABASE_FILE" <<SQL | while read -r line; do
.mode list
.separator "$DELIM"
SELECT
    $common_query
FROM $table
WHERE $where
$order
$limit;
SQL
            id="${line%%$DELIM*}"
            rest="${line#*$DELIM}"
            status="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            full_command="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            cwd="${rest%%$DELIM*}"
            rest="${rest#*$DELIM}"
            duration="${rest%%$DELIM*}"
            relative_time="${rest#*$DELIM}"
            print_history_record_verbose "$id" "$status" "$full_command" "$cwd" "$duration" "$relative_time"
        done
    fi

}

###############################################################################
# 7. Helper for UTC date/time in queries
###############################################################################
htime() {
    local user_time="$*"

    # Convert user input to an explicit "YYYY-MM-DD HH:MM:SS" format in local time
    local local_time=$(date -d "$user_time" "+%Y-%m-%d %H:%M:%S")

    # Convert that formatted local time to UTC
    local utc_time=$(date -u -d "$local_time" "+%Y-%m-%d %H:%M:%S")

    echo "$utc_time"
}

###############################################################################
# 8. Method to run a command from history
###############################################################################

hselect() {
    # Determine if the --help flag was passed and if so bail out
    if [[ "$*" == *--help* ]]; then
        return
    fi

    local selected_id=""

    # If an --id flag is passed, extract its value
    if [[ "$*" == *--id* ]]; then
        while [[ $# -gt 0 ]]; do
            case "$1" in
                --id)
                    shift  # Move to the next argument
                    selected_id="$1"
                    break
                    ;;
                *)
                    shift  # Continue parsing arguments
                    ;;
            esac
        done

        # Ensure selected_id is not empty
        if [[ -z "$selected_id" ]]; then
            echo "Error: No ID provided!" >&2
            return 1
        fi
    else
        # Show history with IDs using existing hquery logic
        hquery "$@"

        # Get desired ID from user
        read -rp "Enter command ID to recall: " selected_id
    fi

    # Ensure selected_id is not empty
    if [[ -z "$selected_id" ]]; then
        echo "Error: No ID selected, exiting."
        return
    fi

    # 4. Fetch command by ID and put it in prompt
    local selected_command="$(hquery --id "$selected_id")"

    # 5. Ask the user if they want to run the command (print command)
    echo -e "\nSelected command: $selected_command"
    read -rp "Run this command? [y/N] " confirm
    [[ "$confirm" =~ ^[Yy] ]] || return

    # 6. Execute the selected command
    eval "$selected_command"
}

hreadme() {
    cat <<EOF
########################################################################
# Usage Examples:
#
#   hquery --after "1 hour ago"                     # Show last hour
#   hquery --failed --fast                          # Show failed commands, order by duration asc
#   hquery --exit 127 'no-such*'
#   hquery --success 'make*'
#   hquery --canceled
#   hquery --after "yesterday 14:00" --before "yesterday 16:00" '*deploy*'
#
#   # Filter by directory:
#   hquery --cwd                                     # uses current directory (PWD)
#   hquery --cwd /path/to/dir
#
#   # Synonyms for success/fail/cancel, e.g.:
#   hquery --succeeded
#   hquery --fail
#   hquery --cancelled
#
#   # conveinence filters
#   hquery --mru               # newest last
#   hquery --lru               # oldest last
#   hquery --mfu               # largest usage last
#   hquery --lfu               # smallest usage last
#
#   # Combine grouping with other filters, e.g. top used commands in the last hour:
#   hquery --mfu --after "1 hour ago"
#
#   # Note: 
#   #   The same semantics work for the hselect command which can be used
#   #   to run a command from history.
#
########################################################################
EOF
}

source "$DOTFILES_DIR/colors.sh"

print_history_record_verbose() {
    local id="$1"
    local status="$2"
    local command_text="$3"
    local cwd="$4"
    local duration="$5"
    local relative_time="$6"

    # Choose the colored circle.
    local circle="●"
    local status_color="${FG_WHITE}"
    if [[ "$status" == *"Success"* ]]; then
        status_color="${FG_GREEN}"
    elif [[ "$status" == *"Cancelled"* ]]; then
        status_color="${FG_YELLOW}"
    elif [[ "$status" == *"Failed"* ]]; then
        status_color="${FG_RED}"
    fi

    printf "\n${status_color}%b ${RESET_COLOR}${DIM}${FG_WHITE}%s >${RESET_COLOR} ${BOLD}%s${RESET_COLOR}\n" "$circle" "$id" "$command_text"
    printf "└── ${status_color}%-5s${FG_WHITE}${FG_CYAN}%8s${FG_WHITE}${DIM} · ${FG_WHITE}%s${RESET_COLOR}\n" "$duration" "$relative_time" "$cwd"
    # printf "└── ${status_color}%-5s${FG_WHITE}${FG_CYAN}%8s${FG_WHITE}${DIM} · ${status_color}%-14s ${FG_WHITE}· %s${RESET_COLOR}\n" "$duration" "$relative_time" "$status" "$cwd"
}
