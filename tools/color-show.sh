#!/usr/bin/env bash

source "${DOTFILES_DIR}/colors.sh"

echo -e "${DIM}${FG_CYAN}Dim Cyan${RESET_COLOR}"

echo -e "${BOLD}${FG_CYAN}abc${RESET_COLOR}@${BOLD}${FG_MAGENTA}autocode${RESET_COLOR} · ${DIM}${FG_GREEN}(bash ♦ docker)${RESET_COLOR} · ${FG_YELLOW}$(pwd)${RESET_COLOR} · git:(${FG_GREEN}loki/modernization${RESET_COLOR}) ${FG_RED}✗%${RESET_COLOR}"

# Array of formatting styles
styles=("" "$BOLD" "$DIM")
style_names=("Normal" "Bold" "Dim")

# Array of foreground colors
fg_colors=("$FG_BLACK" "$FG_RED" "$FG_GREEN" "$FG_YELLOW" "$FG_BLUE" "$FG_MAGENTA" "$FG_CYAN" "$FG_WHITE")
fg_names=("Black" "Red" "Green" "Yellow" "Blue" "Magenta" "Cyan" "White")

# Print all foreground color permutations
echo -e "\nForeground Colors:"
for i in "${!fg_colors[@]}"; do
    for j in "${!styles[@]}"; do
        echo -e "${styles[j]}${fg_colors[i]}${style_names[j]} ${fg_names[i]}${RESET_COLOR}"
    done
    echo # Add spacing between color groups
done

# Array of background colors
bg_colors=("$BG_BLACK" "$BG_RED" "$BG_GREEN" "$BG_YELLOW" "$BG_BLUE" "$BG_MAGENTA" "$BG_CYAN" "$BG_WHITE")
bg_names=("Black" "Red" "Green" "Yellow" "Blue" "Magenta" "Cyan" "White")

# Print all background colors separately
echo -e "\nBackground Colors:"
for i in "${!bg_colors[@]}"; do
    echo -e "${bg_colors[i]} Background ${bg_names[i]} ${RESET_COLOR}"
done

variable=$(cat <<EOF
This is a multi-line string.
You can include variables like $HOME or commands like $(date).
Could it be more simple?
EOF
)

echo "$variable"
