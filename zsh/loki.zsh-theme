local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
local separator="________________________________________________________________________________
"

PROMPT='${separator}| [%*]%{$fg_bold[green]%}%p %{$fg_bold[cyan]%}%n %{$fg[white]%}@ %{$fg_bold[green]%}%m %{$fg[white]%}in %{$fg_bold[yellow]%}%0~ %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}
| ${ret_status} %{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
