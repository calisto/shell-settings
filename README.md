# Loki Shell Extensions

A collection of shell extensions and utilities that enhance your command-line experience across different platforms (Linux, macOS, Windows).

## Features

- Cross-platform shell configuration (bash, zsh)
- Enhanced command-line utilities and aliases
- Customized prompt with environment detection
- Extensive bash history configuration
- Nano editor configuration with syntax highlighting
- SSH configuration for remote sessions
- TMux integration
- Git configuration and utilities

## Installation

1. Clone the repository:
```bash
git clone https://github.com/yourusername/loki-shell-extensions.git
cd loki-shell-extensions
```

2. Run the install script:
```bash
./install.sh
```

The install script will:
- Install files to `~/.local/share/loki-shell-extensions` (following XDG Base Directory specification)
- Create backups of existing configurations in `~/.local/share/loki-shell-extensions-backup/[timestamp]`
- Set up symlinks for various configuration files
- Configure shell startup files (bashrc, bash_profile, zshrc if applicable)

## Technical Details

The shell extensions are configured through environment variables:
- `QSHELL_SETTINGS_LOCATION`: Points to the installation directory (`~/.local/share/loki-shell-extensions`)

The system automatically detects your OS type (Linux, macOS, Windows/WSL) and loads the appropriate extensions.

## Features and Helpers

### Shell Navigation
- `cd`, `back`, `flip`: Enhanced directory navigation with stack
- `..`, `...`, `.3`, `.4`, `.5`, `.6`: Quick directory traversal
- `lr`: Full recursive directory listing
- `mcd`: Create directory and change into it

### File Operations
- `extract`: Universal archive extraction
- `zipf`: Create ZIP archive of a folder
- `numFiles`: Count files in current directory
- `cleanupDS`: Clean .DS_Store files

### System Information
- `ii`: Display system information
- `myip`: Show public IP address
- `path`: Show executable paths
- `show_options`: Display bash options

### Process Management
- `findPid`: Find process ID
- `memHogsTop`, `memHogsPs`: Memory usage analysis
- `cpu_hogs`: CPU usage analysis
- `topForever`: Continuous system monitoring
- `my_ps`: List user processes

### Network Utilities
- `httpHeaders`: Get webpage headers
- `httpDebug`: Debug webpage loading
- `netCons`: Show TCP/IP connections
- `openPorts`: Show listening ports
- `lsock`, `lsockU`, `lsockT`: Socket information

### File Search
- `qfind`: Quick file search
- `ff`: Find file in current directory
- `ffs`: Find file with name starting with
- `ffe`: Find file with name ending with

### History
- Nearly infinite bash history
- History stored in `~/.q_bash_history`
- Timestamp for each command
- Optional SQLite-based history tracking (if sqlite3 is installed)

## Platform-Specific Features

The system automatically loads additional configurations based on your platform:
- macOS: Additional terminal and system integration
- Windows/WSL: Git-bash and WSL-specific configurations
- Linux: Standard configurations

## Updating

Since the configuration uses symlinks, updating is as simple as:
1. `cd` to your cloned repository
2. `git pull`
3. `./install.sh` (will update symlinks if needed)

## Contributing

Feel free to submit issues and pull requests for bug fixes or new features.